```
Filename: 347-domain-separation.md
Title: Domain separation for certificate signing keys
Author: Nick Mathewson
Created: 19 Oct 2023
Status: Open
```

## Our goal

We'd like to be able to use the "family key" from proposal 321 as a
general purpose signing key, to authenticate other things than the
membership of a family.  For example, we might want to have a
challenge/response mechanism where the challenger says, "If you want
to log in as the owner of the account corresponding to this family,
sign the following challenge with your key.  Or we might want to
have a message authentication scheme where an operator can
sign a message in a way that proves key ownership.

We _might_ also like to use relay identity keys or onion service
identitiy keys for the same purpose.

## The problem

When we're using a signing key for two purposes, it's important
to perform some kind of domain separation so that documents
signed for one purpose can't be mistaken for documents signed for the
other.

For example, in the challenge/response example, it would be bad if
the challenger could provide a challenge string that would cause the
signer to inadvertently authenticate an incorrect family.

These keys are currently used in some places with no
personalization. Their signature format is as described in
`cert-spec.txt`, which says:

>  The signature is created by signing all the fields in
>  the certificate up until "SIGNATURE" (that is, signing
>  sizeof(ed25519_cert) - 64 bytes).



## One solution

This one is pretty easy: we would extend `cert-spec` as follows.


> ### Using signing keys for other purposes.
>
> Other protocols may introduce other uses for the signing keys
> in these certificates other than those specified here.  When
> they do, they MUST ensure that the documents being signed
> cannot be confused with the certificate bodies of this document.
>
> In some existing cases in the Tor protocols, we achieve this by
> specifying an ASCII prefix string that must be prepended to the
> other protocol's signed object before it is signed.
>
> For future protocols, we recommend that this be done by
> specifying that the signing key is to be used to sign a
> cSHAKE digest (or other secure customizable digest)
> of the other protocol's signed object, using
> a customization string unique to the other protocol.

We would also make this amendment:

> ### Future versions of this specification
>
> In order to maintain the domain separation that currently exists
> between the signatures on these certificates and other documents
> signed with the same keys, it suffices (for now!) that these
> certificates begin with the version byte `[01]`, whereas
> the other documents are in printable ASCII, which never
> includes `[01]`.
>
> Future versions of this specification SHOULD move to using
> an ed25519-prehash construction, using a customizable hash
> with built-in domain separation.

