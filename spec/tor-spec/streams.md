<a id="tor-spec.txt-6"></a>

# Application connections and stream management{#application-connections-and-stream-management}

This section describes how clients use relay messages to communicate with
exit nodes, and how use this communication channel to send and receive
application data.
